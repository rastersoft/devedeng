#!/bin/sh

rm -f devede.pot
xgettext -f POTFILES.in -o devede.pot
for pofile in *.po; do
        msgmerge -N "${pofile}" devede.pot > tmp.po
        mv tmp.po "${pofile}"
done