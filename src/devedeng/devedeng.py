#!/usr/bin/env python3

# Copyright 2014 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of DeVeDe-NG
#
# DeVeDe-NG is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License.
#
# DeVeDe-NG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import gi
gi.require_version('Gtk', '3.0')
import sys
import gettext
import locale
from gi.repository import Gtk

import devedeng.project
import devedeng.configuration_data
import devedeng.choose_disc_type

_ = gettext.gettext

config_data = devedeng.configuration_data.configuration.get_config()

if config_data is None:
    print("Can't locate extra files. Aborting.")
    sys.exit(1)

Gtk.init(sys.argv)

gettext.install(config_data.gettext_domain, localedir=config_data.share_locale)
locale.setlocale(locale.LC_ALL, "")
locale.bindtextdomain(config_data.gettext_domain, config_data.share_locale)
locale.bind_textdomain_codeset(config_data.gettext_domain, "UTF-8")
locale.textdomain(config_data.gettext_domain)

def py():
    global config_data

    mwindow = devedeng.project.devede_project()
    ask_type = devedeng.choose_disc_type.choose_disc_type()

    Gtk.main()
    config_data.save_config()
