#!/usr/bin/env python3

# Copyright 2014 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of DeVeDe-NG
#
# DeVeDe-NG is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License.
#
# DeVeDe-NG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os
import devedeng.configuration_data
import devedeng.executor


class file_copy(devedeng.executor.executor):

    def __init__(self, input_path, output_path):

        devedeng.executor.executor.__init__(self)
        self.config = devedeng.configuration_data.configuration.get_config()

        self.text = _("Copying file %(X)s") % {
            "X": os.path.basename(input_path)}

        self.command_var = ["dd", f"if={input_path}", f"of=output_path", "status=progress", "bs=1048576"]
        self.file_size = float(os.stat(input_path).st_size)
        print("Paso 1")

    def process_stdout(self, data):
        return

    def process_stderr(self, data):
        if (data is None) or (len(data) == 0):
            return
        parts = data[0].split(" ")
        if (len(parts) < 2) or (parts[1] != "bytes"):
            return

        try:
            copied = float(parts[0])
        except:
            return

        p = copied / self.file_size
        self.progress_bar[1].set_fraction(p)
        self.progress_bar[1].set_text("%.1f%%" % (p * 100))
